package com.kingfeng.batteryplugin;

import java.util.Random;

import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;
import io.flutter.plugin.common.PluginRegistry.Registrar;

/**
 * BatteryPlugin
 */
public class BatteryPlugin implements MethodCallHandler {
    /**
     * Plugin registration.
     */
    public static void registerWith(Registrar registrar) {
        final MethodChannel channel = new MethodChannel(registrar.messenger(), "battery_plugin");
        channel.setMethodCallHandler(new BatteryPlugin());

        final MethodChannel methodChannel = new MethodChannel(registrar.messenger(), "battery");
        methodChannel.setMethodCallHandler(new BatteryPlugin());
    }

    @Override
    public void onMethodCall(MethodCall call, Result result) {
        if (call.method.equals("getPlatformVersion")) {
            result.success("Android " + android.os.Build.VERSION.RELEASE);
        } else if (call.method.equals("getBatteryLevel")) {
            Random random = new Random();
            result.success(random.nextInt(100));
            // 这里我们就添加上调用原生的方法；

        } else {
            result.notImplemented();
        }
    }
}
