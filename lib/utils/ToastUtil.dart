import 'package:fluttertoast/fluttertoast.dart';

// 对第三方的 Fluttertoast 进行封装使用, https://pub.dartlang.org/packages/fluttertoast
// author: kingfeng
// date: 2018-9-6
class ToastUtil {
  // toast
  static showToast(String text) {
    Fluttertoast.showToast(
        msg: "$text",
        gravity: ToastGravity.CENTER,
        timeInSecForIos: 1,
        bgcolor: "#99000000",
        textcolor: '#ffffff');
  }
}
