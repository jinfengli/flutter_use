import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_use/third.dart';

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      debugShowCheckedModeBanner: false,    // 隐藏右上角的debug标识
      title: 'FlutterDemo',
      theme: new ThemeData(
//        primarySwatch: Colors.blue, // 并不是一个Color，而是一个MaterialColor
          primaryColor: Colors.red
      ),

      home: new ThirdHomePage("third homepage"),
    );
  }
}