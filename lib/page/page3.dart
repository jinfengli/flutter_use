import 'dart:async';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:device_info/device_info.dart';
import 'package:flutter/services.dart';

import 'package:battery_plugin/battery_plugin.dart';

// 这里是第3个页面
class Page3 extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => Page3State();
}

class Page3State extends State<Page3> {
  var _deviceData;

  @override
  void initState() {
    super.initState();

    getDeviceInfo();
    getPlatformVersion();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: new Center(
          child: new Column(
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          new Text(
            "plugin(device_info) using ：" + '\n' + " $_deviceData",
            style: new TextStyle(
                fontWeight: FontWeight.bold,
                color: Colors.black54,
                fontSize: 16.0),
          ),
        ],
      )),
    );
  }

  Future getDeviceInfo() async {
    DeviceInfoPlugin deviceInfo = new DeviceInfoPlugin();
    if (Platform.isAndroid) {
      AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
      print('Running on ${androidInfo.model}'); // e.g. "Moto G (4)"
      _deviceData = androidInfo.model +
          '\n' +
          androidInfo.brand +
          '\n' +
          androidInfo.manufacturer +
          '\n' +
          androidInfo.display;

      if (androidInfo.isPhysicalDevice) {
        _deviceData = _deviceData + "-物理设备";
      }
      print(androidInfo.isPhysicalDevice);
      print(androidInfo.brand);
    } else if (Platform.isIOS) {
      IosDeviceInfo iosInfo = await deviceInfo.iosInfo;
      print('Running on ${iosInfo.utsname.machine}'); // e.g. "iPod7,1"
      _deviceData = iosInfo.model;
    }
  }

  void getPlatformVersion() async {
    String platformVersion;
    String batteryLevel;
    // Platform messages may fail, so we use a try/catch PlatformException.
    // 要引入  flutter/service.dart 库
    try {
      platformVersion = await BatteryPlugin.platformVersion;
      print("使用本地plugin：patformVersion-->" + platformVersion);
    } on PlatformException {
      platformVersion = 'Failed to get platform version.';
    }

    batteryLevel = await BatteryPlugin.batteryLevel;
    print("使用本地plugin：batteryLevel: -->" + batteryLevel);
  }
}
