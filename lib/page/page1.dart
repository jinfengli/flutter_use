import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

// 这里是第二个页面
class Page1 extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => Page1State();
}

class Page1State extends State<Page1> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: new Center(
          child: new Column(
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          new Text(
            "图片test",
            style: new TextStyle(
                fontWeight: FontWeight.bold,
                color: Colors.black54,
                fontSize: 12.0),
          ),

          Padding(
            padding: EdgeInsets.all(20.0),
            child: new Image.network(
                'http://or7hgmr1x.bkt.clouddn.com/as-sonar-properties.png'),
          ),
        ],
      )),
    );
  }
}
