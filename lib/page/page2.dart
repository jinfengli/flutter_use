import 'dart:async';
import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_use/network/HttpManager.dart';
import 'package:flutter_use/seocnd.dart';
import 'package:flutter_use/utils/ToastUtil.dart';

// 这里是第二个页面
class PageTopic extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => PageTopicState();
}

class PageTopicState extends State<PageTopic> {
  var _responseListData; // 返回的数据

  @override
  void initState() {
    getLatestData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var content;
    if (_responseListData == null || _responseListData.isEmpty) {
      content = new Center(child: new CircularProgressIndicator());
    } else {
      content = new ListView.builder(
        padding: const EdgeInsets.only(
          top: 20.0,
          bottom: 20.0,
        ),

        physics: AlwaysScrollableScrollPhysics(),
        itemCount: _responseListData.length,
//        controller: _scrollController,
        itemBuilder: buildCardItem,
      );
    }

    var _refreshIndicator = new RefreshIndicator(
      onRefresh: _refreshData,
      child: content,
    );
    return _refreshIndicator;
  }

  void getLatestData() {
    HttpManager.get("http://www.wanandroid.com/friend/json").then((response) {
      if (response != null && response.isNotEmpty) {
        Map<String, dynamic> resultMap = jsonDecode(response);
        var datas = resultMap["data"];
        print(datas);
        if (resultMap["errorCode"] == 0 && datas != null) {
          setState(() {
            _responseListData = datas;
          });
        } else {
          print("${resultMap["errorMsg"]}");
        }
      }
    });
  }

  Widget buildCardItem(BuildContext context, int index) {
    var item = _responseListData[index];
    var name = item["name"];
    var url = item['link'];

    return new GestureDetector(
      onTap: () {
        ToastUtil.showToast(name);
        Navigator.push(
              context,
              new MaterialPageRoute(builder: (context) => new SecondPageState()),
            );
      },

      child: new Card(
        child: Wrap(children: <Widget>[
          Padding(
//                padding: EdgeInsets.all(10.0),
            padding: const EdgeInsets.only(
                left: 30.0, top: 15.0, right: 20.0, bottom: 15.0),

            child: Text(
              name + "--" + url,
              softWrap: false,
              overflow: TextOverflow.fade
            ),
          )
        ]),
      ),
    );
  }

  Future<void> _refreshData() async {
    getLatestData();
    return null;
  }
}
