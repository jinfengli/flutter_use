
import 'dart:async';
import 'package:http/http.dart' as http;

class HttpManager {

  ///  get 请求
  static Future<String> get(String url, {Map<String, String> params}) async {
    print("HttpManager, url is: " + url);
    if (params != null && params.isNotEmpty) {
      StringBuffer sb = new StringBuffer("?");
      params.forEach((key, value) {
        sb.write("$key" + "=" + value + "&");
      });
      String paramsString = sb.toString();
      paramsString = paramsString.substring(0, paramsString.length - 1);
      url += paramsString;
    }

    /// 这里如果有能用请求头信息需要设置的话可以在这里传。
    /// 这里需要设置一下cookie 、、 从缓存取出来吧。
//    var headMap = Map<String, String>();
//    headMap["Cookie"] = AppConstant.APP_COOKIE;
    var response = await http.get(url/*, headers: headMap*/);
    print("response body:  " + response.body);
    return response.body;
  }

}