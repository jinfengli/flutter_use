import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_use/page/page1.dart';
import 'package:flutter_use/page/page2.dart';
import 'package:flutter_use/page/page3.dart';
import 'package:flutter_use/utils/BottomNavigation.dart';

//  page 3
class ThirdHomePage extends StatefulWidget {

  var name;

  ThirdHomePage(this.name);

  @override
  State<StatefulWidget> createState() => ThirdHomePageState();
}

var _body;
var _currentBottomIndex = 0;

class ThirdHomePageState extends State<ThirdHomePage> {
  var _bottomTitles1 = ["首页", "分类", "U惠购"];
  var _bottomTitles = ["", "", ""];

  var _bottomIconNor = [
    "res/images/toolbar_home_normal.png",
    "res/images/toolbar_categories_normal.png",
    "res/images/toolbar_brand_normal.png"
  ];

  var _bottomIconSelected = [
    "res/images/toolbar_home_selected.png",
    "res/images/toolbar_categories_selected.png",
    "res/images/toolbar_brand_selected.png"
  ];

  @override
  Widget build(BuildContext context) {
    _body = IndexedStack(
      children: <Widget>[
        Page1(),
        PageTopic(),
        Page3()
      ],
      index: _currentBottomIndex,
    );

    return Scaffold(
      appBar: AppBar(
        title: Text(
          _bottomTitles1[_currentBottomIndex],
          style: TextStyle(color: Colors.white),
        ),
//        iconTheme: IconThemeData(color: Colors.red, opacity: 0.5, size: 15.5),
        leading: null,
        automaticallyImplyLeading: false,// 隐藏返回按钮
        centerTitle: true,

      ),
      bottomNavigationBar: BottomNavigationBar(
        items: getBottomNavigationBarItems(),
        currentIndex: _currentBottomIndex,
        onTap: (index) {
          setState(() {
            _currentBottomIndex = index;
          });
        },
      ),
      body: _body,
    );
  }


  // new Container(height: 0.0)   使用这个是为了不想设置Items 的title，传Text("") 文字仍然占位
  // 使用这种方式之后，占位消失，但是按下的时候底部有多余的填充物,可以看github回答
  // https://github.com/flutter/flutter/issues/17099#issuecomment-385870942
 getBottomNavigationBarItems() => List.generate(
      _bottomTitles.length,
      (index) => BottomNavigationBarItem(
          icon: getBottomIcon(index), title: new Container(height: 0.0)));

  getBottomTitle(int i) => Text(
        _bottomTitles[i],
        style: TextStyle(
            color: _currentBottomIndex == i ? Colors.red : Colors.grey),
      );

  getBottomIcon(int i) => Image.asset(
        getBottomIconPath(i),
        width: 35.0,
        height: 35.0,
      );

  String getBottomIconPath(int i) =>
      _currentBottomIndex == i ? _bottomIconSelected[i] : _bottomIconNor[i];
}
