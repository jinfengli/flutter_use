import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_use/utils/ToastUtil.dart';

// 这里是第二个页面
class SecondPageState extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    return new Scaffold(
      appBar: new AppBar(
        title: new Text("item detail page"),
//        automaticallyImplyLeading: false,
        centerTitle: true,
      ),

      body: new Center(
        child: new RaisedButton(
          onPressed: () {
//            Navigator.pop(context);
            ToastUtil.showToast("dajiahoa");
          },
          child: new Text('Go back!'),
        ),
      ),
    );
  }
}